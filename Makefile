ROOT = $HOME/src/emacs-27.1
CC = gcc
LD = gcc
CFLAGS = -ggdb3 -Wall
LDFLAGS =

all: mymod.so

%.so: %.o
	$(LD) -shared $(LDFLAGS) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) -I$(ROOT)/src -fPIC -c $<
