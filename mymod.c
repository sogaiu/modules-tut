#include <emacs-module.h>

int plugin_is_GPL_compatible;

static emacs_value
Fmymod_test(emacs_env *env, ptrdiff_t nargs, emacs_value args[], void *data)
{
  return env->make_integer(env, 42);
}

static void
bind_function(emacs_env *env, const char *name, emacs_value Sfun)
{
  emacs_value Qfset = env->intern(env, "fset");
  emacs_value Qsym = env->intern(env, name);

  emacs_value args[] = { Qsym, Sfun };

  env->funcall(env, Qfset, 2, args);
}

static void
provide(emacs_env *env, const char *feature)
{
  emacs_value Qfeat = env->intern(env, feature);
  emacs_value Qprovide = env->intern(env, "provide");
  emacs_value args[] = { Qfeat };

  env->funcall(env, Qprovide, 1, args);
}

int
emacs_module_init(struct emacs_runtime *ert)
{
  emacs_env *env = ert->get_environment(ert);

  emacs_value fun = env->make_function(env,
                                       0,
                                       0,
                                       Fmymod_test,
                                       "doc",
                                       NULL
                                       );

  bind_function(env, "mymod-test", fun);
  provide(env, "mymod");

  return 0;
}
